#include "gtest/gtest.h"
#include "../../src/sections_interval.hpp"
#include <list>

const auto nneg = QuantileBlock<IntervalOb>::NONNEGATIVE;
const auto npos = QuantileBlock<IntervalOb>::NONPOSITIVE;


TEST(SectionsInterval, AddOneBlock) {

  auto si = SectionsInterval<QuantileBlock, IntervalOb>(1);

  // initialize blocks and blocks interval
  auto block1 = QuantileBlock<IntervalOb>(nneg);
  block1.set_quantile(0.5);
  std::list<IntervalOb> obs_list = {IntervalOb({0,0,10})};
  block1.add_obs(obs_list.begin(), obs_list.end());

  t_sidx new_sidx1 = si.set_next_section(0, block1);
  EXPECT_EQ(new_sidx1, 0);

  t_sidx new_sidx2 = si.set_next_section(0, block1);
  EXPECT_EQ(new_sidx2, 1);
}
