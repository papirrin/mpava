#include "gtest/gtest.h"
#include "../../src/mod_pava.hpp"

#include <list>

TEST(ModPavaEndToEnd, OneQuantile) {
  typedef Pava<SectionsInterval<QuantileBlock, ListOb> > PavaType;
  auto qs = PavaType::QuantilesType{{0.5}};
  auto  pava = PavaType{qs};

  std::list<IntervalOb> obs_list = {
    IntervalOb({0, 0, 10}),
    IntervalOb({0, 0, 5}),
    IntervalOb({0, 0, 2}),
    IntervalOb({0, 1, 4}),
    IntervalOb({0, 1, 0}),
    IntervalOb({0, 2, 4}),
    IntervalOb({1, 2,10}),
    IntervalOb({3, 4, 3})
  };
  pava.do_pava(obs_list.begin(), obs_list.end());

  std::vector<std::vector<t_value> > results;
  pava.extract_results(results);

  std::vector<std::vector<t_value> > expected;
  expected.push_back(std::vector<t_value>({4, 0, 10, 0, 3}));
  EXPECT_EQ(expected, results);
}

TEST(ModPavaEndToEnd, ThreeAlmostEqualQuantiles) {
  typedef Pava<SectionsInterval<QuantileBlock, ListOb> > PavaType;
  auto  pava = PavaType(PavaType::QuantilesType({0.5, 0.500001, 0.500002}));

  std::list<IntervalOb> obs_list = {
    IntervalOb({0, 0, 10}),
    IntervalOb({0, 0, 5}),
    IntervalOb({0, 0, 2}),
    IntervalOb({0, 1, 4}),
    IntervalOb({0, 1, 0}),
    IntervalOb({0, 2, 4}),
    IntervalOb({1, 2,10}),
    IntervalOb({3, 4, 3})
  };
//  std::cout << "Do pava before error" << std::endl;
  pava.do_pava(obs_list.begin(), obs_list.end());
//  std::cout << "afetr Do pava" << std::endl;

  std::vector<std::vector<t_value> > results;
  pava.extract_results(results);

  std::vector<std::vector<t_value> > expected;
  expected.push_back(std::vector<t_value>({4, 0, 10, 0, 3}));
  expected.push_back(std::vector<t_value>({4, 0, 10, 0, 3}));
  expected.push_back(std::vector<t_value>({4, 0, 10, 0, 3}));
  EXPECT_EQ(expected, results);

  pava.print_sections_intervals();
}

TEST(ModPavaEndToEnd, IndepdentObservations) {
  typedef Pava<SectionsInterval<QuantileBlock, ListOb> > PavaType;
  auto  pava = PavaType(PavaType::QuantilesType({0, 0.5, 1}));

  std::list<IntervalOb> obs_list = {
    IntervalOb({0, 0, 10}),
    IntervalOb({0, 0, 5}),
    IntervalOb({0, 0, 0}),

    IntervalOb({1, 1, 8}),
    IntervalOb({1, 1, 5}),
    IntervalOb({1, 1, 2}),

    IntervalOb({2, 2, 7}),
    IntervalOb({2, 2, 5}),
    IntervalOb({2, 2, 3}),

    IntervalOb({4, 4, 7}),
    IntervalOb({4, 4, 4}),
    IntervalOb({4, 4, 4})

  };
  pava.print_sections_intervals();

//  std::cout << "Do pava before error" << std::endl;
  pava.do_pava(obs_list.begin(), obs_list.end());
//  std::cout << "afetr Do pava" << std::endl;

  std::vector<std::vector<t_value> > results;
  pava.extract_results(results);

  std::vector<std::vector<t_value> > expected;
  expected.push_back(std::vector<t_value>({0, 2, 3, 0, 4}));
  expected.push_back(std::vector<t_value>({5, 5, 5, 0, 4}));
  expected.push_back(std::vector<t_value>({10, 8, 7, 0, 7}));
  EXPECT_EQ(expected, results);

//  pava.print_sections_intervals();
}


TEST(ModPavaEndToEnd, CrossingEvenlopeAndMedian) {
  typedef Pava<SectionsInterval<QuantileBlock, ListOb> > PavaType;
  auto  pava = PavaType(PavaType::QuantilesType({0, 0.5, 1}));

  std::list<IntervalOb> obs_list = {
    IntervalOb({0, 0, 10}),
    IntervalOb({0, 0, 5}),
    IntervalOb({0, 0, 0}),

    IntervalOb({0, 1, 8}),
    IntervalOb({0, 1, 5}),
    IntervalOb({0, 1, 2}),

    IntervalOb({1, 2, 7}),
    IntervalOb({1, 2, 5}),
    IntervalOb({1, 2, 3}),

    IntervalOb({2, 4, 7}),
    IntervalOb({2, 4, 4}),
    IntervalOb({2, 4, 4})

  };
  pava.print_sections_intervals();

//  std::cout << "Do pava before error" << std::endl;
  pava.do_pava(obs_list.begin(), obs_list.end());
//  std::cout << "afetr Do pava" << std::endl;

  std::vector<std::vector<t_value> > results;
  pava.extract_results(results);

  std::vector<std::vector<t_value> > expected;
  expected.push_back(std::vector<t_value>({ 0, 0, 3, 0, 0}));
  expected.push_back(std::vector<t_value>({ 5, 0, 5, 0, 0}));
  expected.push_back(std::vector<t_value>({10, 0, 7, 0, 0}));
  EXPECT_EQ(expected, results);

//  pava.print_sections_intervals();
}



TEST(ModPavaEndToEnd, ThreeQuantiles) {
  typedef Pava<SectionsInterval<QuantileBlock, ListOb> > PavaType;
  auto  pava = PavaType(PavaType::QuantilesType({0.2, 0.6, 0.7}));

  std::list<IntervalOb> obs_list = {
    IntervalOb({0, 0, 10}),
    IntervalOb({0, 0, 5}),
    IntervalOb({0, 0, 2}),
    IntervalOb({0, 1, 4}),
    IntervalOb({0, 1, 0}),
    IntervalOb({0, 2, 4}),
    IntervalOb({1, 2,10}),
    IntervalOb({3, 4, 3})
  };
  pava.do_pava(obs_list.begin(), obs_list.end());

//  EXPECT_EQ(5, block.calc_value(si));

  pava.print_sections_intervals();
}


TEST(ModPavaEndToEnd, TwoQuantiles) {
  typedef Pava<SectionsInterval<QuantileBlock, ListOb> > PavaType;
  auto  pava = PavaType(PavaType::QuantilesType({0.2, 0.7}));

  std::list<IntervalOb> obs_list = {
    IntervalOb({0, 0, 10}),
    IntervalOb({0, 0, 5}),
    IntervalOb({0, 0, 2}),
    IntervalOb({0, 1, 4}),
    IntervalOb({0, 1, 0}),
    IntervalOb({0, 2, 4}),
    IntervalOb({1, 2,10}),
    IntervalOb({3, 4, 3})
  };
  pava.do_pava(obs_list.begin(), obs_list.end());

//  EXPECT_EQ(5, block.calc_value(si));

  pava.print_sections_intervals();
 
  std::cout << "******************************************" << std::endl;
  std::vector<std::vector<t_value> > results;

  size_t nr_sections = pava.extract_results(results);
  size_t nr_qs = results.size();

  std::cout << "nr_qs: " << nr_qs << "\tnr_section: " << nr_sections << std::endl;

  for(size_t input_sidx = 0; input_sidx < nr_sections; ++input_sidx) {
      std::cout << input_sidx << ": ";
    for(size_t qidx = 0; qidx < nr_qs; ++qidx) {
      std::cout << results[qidx][input_sidx] << ", ";
    }
    std::cout << std::endl;
  }
}



TEST(ModPavaEndToEnd, OneQuantile2) {
  typedef Pava<SectionsInterval<QuantileBlock, ListOb> > PavaType;
  auto  pava = PavaType(PavaType::QuantilesType({0.2}));

  std::list<IntervalOb> obs_list = {
    IntervalOb({0, 0, 10}),
    IntervalOb({0, 0, 5}),
    IntervalOb({0, 0, 2}),
    IntervalOb({0, 1, 4}),
    IntervalOb({0, 1, 0}),
    IntervalOb({0, 2, 4}),
    IntervalOb({1, 2,10}),
    IntervalOb({3, 4, 3})
  };
  pava.do_pava(obs_list.begin(), obs_list.end());

//  EXPECT_EQ(5, block.calc_value(si));

  pava.print_sections_intervals();
}


TEST(ModPavaEndToEnd, TwoObservations) {
  typedef Pava<SectionsInterval<QuantileBlock, ListOb> > PavaType;
  auto  pava = PavaType(PavaType::QuantilesType({0.5}));

  std::list<IntervalOb> obs_list = {
    IntervalOb({0, 1, 1}),
    IntervalOb({1, 2, 0}),
  };
  pava.print_sections_intervals();
  pava.do_pava(obs_list.begin(), obs_list.end());

  std::vector<std::vector<t_value> > results;
  pava.extract_results(results);

  std::vector<std::vector<t_value> > expected;
  expected.push_back(std::vector<t_value>({0, 1, 0}));
  EXPECT_EQ(expected, results);
}

TEST(ModPavaEndToEnd, UnorderedObservations) {
  typedef Pava<SectionsInterval<QuantileBlock, ListOb> > PavaType;
  auto  pava = PavaType(PavaType::QuantilesType({0.5}));

  std::list<IntervalOb> obs_list = {
    IntervalOb({1, 2, 1}),
    IntervalOb({2, 3, 1}),
    IntervalOb({1, 6, 138}),
    IntervalOb({1, 7, 226}),
    IntervalOb({1, 4, 251}),
    IntervalOb({3, 8, 288}),
    IntervalOb({4, 5, 93}),
    IntervalOb({1, 9, 338}),
    IntervalOb({5, 10, 34}),
  };
  ASSERT_THROW(pava.do_pava(obs_list.begin(), obs_list.end()), std::logic_error);
}

TEST(ModPavaEndToEnd, ExtremeQuantilesSimData) {
  typedef Pava<SectionsInterval<QuantileBlock, ListOb> > PavaType;
  auto  pava = PavaType(PavaType::QuantilesType({0.0, 1}));

  std::list<IntervalOb> obs_list = {
    IntervalOb({0, 1, 1}),
    IntervalOb({1, 2, 1}),
    IntervalOb({0, 3, 138}),
    IntervalOb({0, 4, 226}),
    IntervalOb({0, 5, 251}),
    IntervalOb({2, 6, 288}),
    IntervalOb({5, 7, 93}),
    IntervalOb({0, 8, 338}),
    IntervalOb({7, 9, 34}),
  };

  pava.do_pava(obs_list.begin(), obs_list.end());
  std::vector<std::vector<t_value> > results;
  pava.extract_results(results);

  pava.print_sections_intervals();

  std::vector<std::vector<t_value> > expected;
  // TODO: Get correct data for this test and the next 
  expected.push_back(std::vector<t_value>({0, 1, 0, 137, 88, 25, 38, 30,  4, 0}));
  expected.push_back(std::vector<t_value>({0, 1, 0, 137, 88, 25, 38, 30, 19, 0}));
  EXPECT_EQ(expected, results);
}

TEST(ModPavaEndToEnd, ExtremeQuantilesSimDataExplicitMidpont) {
  typedef Pava<SectionsInterval<QuantileBlock, ListOb> > PavaType;
  auto  pava = PavaType(PavaType::QuantilesType({0.0, 1}), 1.f);

  std::list<IntervalOb> obs_list = {
    IntervalOb({0, 1, 1}),
    IntervalOb({1, 2, 1}),
    IntervalOb({0, 3, 138}),
    IntervalOb({0, 4, 226}),
    IntervalOb({0, 5, 251}),
    IntervalOb({2, 6, 288}),
    IntervalOb({5, 7, 93}),
    IntervalOb({0, 8, 338}),
    IntervalOb({7, 9, 34}),
  };

  pava.do_pava(obs_list.begin(), obs_list.end());
  std::vector<std::vector<t_value> > results;
  pava.extract_results(results);

  pava.print_sections_intervals();

  std::vector<std::vector<t_value> > expected;
  // TODO: Get correct data for this test and the next 
  expected.push_back(std::vector<t_value>({0, 1, 0, 137, 88, 25, 38, 30,  4, 0}));
  expected.push_back(std::vector<t_value>({0, 1, 0, 137, 88, 25, 38, 30, 19, 0}));
  EXPECT_EQ(expected, results);
}

TEST(ModPavaEndToEnd, FourQuantileSimDataShort) {
  typedef Pava<SectionsInterval<QuantileBlock, ListOb> > PavaType;
  auto  pava = PavaType(PavaType::QuantilesType({0.0, 0.1, 0.65, 0.99, 1.}));

  std::list<IntervalOb> obs_list = {
    IntervalOb({2, 6, 288}),
    IntervalOb({5, 7, 93}),
    IntervalOb({0, 8, 338}),
    IntervalOb({7, 9, 34}),
  };
  pava.do_pava(obs_list.begin(), obs_list.end());

//  EXPECT_EQ(5, block.calc_value(si));

  pava.print_sections_intervals();

  std::vector<std::vector<t_value> > results;
  pava.extract_results(results);

  std::vector<std::vector<t_value> > expected;
  expected.push_back(std::vector<t_value>({0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));
  expected.push_back(std::vector<t_value>({4, 0, 10, 0, 3}));
  expected.push_back(std::vector<t_value>({4, 0, 10, 0, 3}));
  EXPECT_EQ(expected, results);
}

TEST(ModPavaEndToEnd, TwoQuantileSimDataNineObs) {
  typedef Pava<SectionsInterval<QuantileBlock, ListOb> > PavaType;
  auto  pava = PavaType(PavaType::QuantilesType({0.0, 0.1}));

  std::list<IntervalOb> obs_list = {
    IntervalOb({0, 1, 1}),
    IntervalOb({1, 2, 1}),
    IntervalOb({0, 3, 138}),
    IntervalOb({0, 4, 226}),
    IntervalOb({0, 5, 251}),
    IntervalOb({2, 6, 288}),
    IntervalOb({5, 7, 93}),
    IntervalOb({0, 8, 338}),
    IntervalOb({7, 9, 34})
  };
  pava.do_pava(obs_list.begin(), obs_list.end());

//  EXPECT_EQ(5, block.calc_value(si));

  pava.print_sections_intervals();

  std::vector<std::vector<t_value> > results;
  pava.extract_results(results);

  std::vector<std::vector<t_value> > expected;
  expected.push_back(std::vector<t_value>({0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));
  expected.push_back(std::vector<t_value>({4, 0, 10, 0, 3}));
  EXPECT_EQ(expected, results);
}

TEST(ModPavaEndToEnd, TwoQuantileSimDataMismatch) {
  typedef Pava<SectionsInterval<QuantileBlock, ListOb> > PavaType;
  auto  pava = PavaType(PavaType::QuantilesType({0.5}));
   std::list<IntervalOb> obs_list = {
    IntervalOb({0, 0, 4818}),
    IntervalOb({1, 1, 1588}),
    IntervalOb({0, 2, 7189}),
    IntervalOb({3, 3, 421})
  };
  pava.do_pava(obs_list.begin(), obs_list.end());

//  EXPECT_EQ(5, block.calc_value(si));

  pava.print_sections_intervals();

  std::vector<std::vector<t_value> > results;
  pava.extract_results(results);

  std::vector<std::vector<t_value> > expected;
  expected.push_back(std::vector<t_value>({4818, 1588, 783, 421}));
  EXPECT_EQ(expected, results);
}

TEST(ModPavaEndToEnd, TwoQuantileSimDataMismatchMins) {
  typedef Pava<SectionsInterval<QuantileBlock, ListOb> > PavaType;
  auto  pava = PavaType(PavaType::QuantilesType({0}));
   std::list<IntervalOb> obs_list = {
    IntervalOb({0, 0, 12}),
    IntervalOb({0, 1, 28}),
    IntervalOb({0, 2, 24}),
    IntervalOb({1, 3, 14})
  };
  pava.do_pava(obs_list.begin(), obs_list.end());

//  EXPECT_EQ(5, block.calc_value(si));

  pava.print_sections_intervals();

  std::vector<std::vector<t_value> > results;
  pava.extract_results(results);

  std::vector<std::vector<t_value> > expected;
  expected.push_back(std::vector<t_value>({12, 12, 0, 2}));
  EXPECT_EQ(expected, results);
}

