#include "gtest/gtest.h"
#include "../../src/blocks.hpp"
#include "../../src/sections_interval.hpp"

#include <list>

const auto nneg = QuantileBlock<IntervalOb>::NONNEGATIVE;
const auto npos = QuantileBlock<IntervalOb>::NONPOSITIVE;

TEST(BlocksTest, QuantileBlockOneBlock) {

  auto si = SectionsInterval<QuantileBlock, IntervalOb>(1);

  // initialize blocks and blocks interval
  auto block = QuantileBlock<IntervalOb>(nneg);
  block.set_quantile(0.5);

  auto ob1 = IntervalOb({0, 0, 10});
  auto ob2 = IntervalOb({0, 0, 5});
  auto ob3 = IntervalOb({0, 0, 2});

  std::list<IntervalOb> obs_list = {ob1, ob2, ob3};
  block.add_obs(obs_list.begin(), obs_list.end());

  EXPECT_EQ(5, block.calc_value(si));

  // Change desired quantile
  block.set_quantile(.24);
  EXPECT_EQ(2, block.calc_value(si));

}

TEST(BlocksTest, QuantileBlockMultipleBlocks) {

  auto si = SectionsInterval<QuantileBlock, IntervalOb>(2);

  // initialize blocks and blocks interval
  auto block1 = QuantileBlock<IntervalOb>(nneg);
  block1.set_quantile(0.5);

  auto block2 = QuantileBlock<IntervalOb>(nneg, 0.5);

  auto block3 = QuantileBlock<IntervalOb>(nneg);
  block3.set_quantile(0.5);

  auto ob11 = IntervalOb({0, 0, 5});
  auto ob12 = IntervalOb({0, 0, 6});
  auto ob13 = IntervalOb({0, 0, 10});

  block1.add_ob(ob11);
  block1.add_ob(ob12);
  block1.add_ob(ob13);
  si.set_next_section(0, block1);

  EXPECT_EQ(6, block1.calc_value(si));

  auto ob21 = IntervalOb({0, 1, 2});
  block2.add_ob(ob21);
  si.set_next_section(0, block2);

  EXPECT_EQ(6, block1.calc_value(si));
  EXPECT_EQ(-4, block2.calc_value(si));

  auto ob31 = IntervalOb({0, 2, 3});
  block3.add_ob(ob31);
  si.set_next_section(1, block3);

  EXPECT_EQ(6, block1.calc_value(si));
  EXPECT_EQ(-4, block2.calc_value(si));
  EXPECT_EQ(3 - 6 - -4, block3.calc_value(si)); // Block2 is invalid so it report 0 to calc_value
}
