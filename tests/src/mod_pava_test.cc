#include "gtest/gtest.h"
#include "../../src/mod_pava.hpp"

#include <list>

const auto nneg = QuantileBlock<ListOb>::NONNEGATIVE;
const auto npos = QuantileBlock<ListOb>::NONPOSITIVE;

typedef Pava<SectionsInterval<QuantileBlock, ListOb> > PavaType;

TEST(ModPava, EmptyQuantiles) {
  auto  pava = PavaType(PavaType::QuantilesType());
}

TEST(ModPava, ToQidxOneQ) {
  auto  pava = PavaType(PavaType::QuantilesType({0.5}));

  EXPECT_EQ(0, pava.to_internal_sidx(0, 0));
  EXPECT_EQ(2, pava.to_internal_sidx(2, 0));
}

TEST(ModPava, ToQidxThreeQ) {
  auto  pava = PavaType(PavaType::QuantilesType({0.1, 0.5, 0.9}));

  EXPECT_EQ(0, pava.to_internal_sidx(0, 0));
  EXPECT_EQ(1, pava.to_internal_sidx(0, 1));
  EXPECT_EQ(2, pava.to_internal_sidx(0, 2));

  EXPECT_EQ(3, pava.to_internal_sidx(1, 0));
  EXPECT_EQ(4, pava.to_internal_sidx(1, 1));
  EXPECT_EQ(5, pava.to_internal_sidx(1, 2));
}

TEST(ModPava, CreateQuantileSidxsOneQ) {
  auto  pava = PavaType(PavaType::QuantilesType({0.5}));

  auto sidxs = pava.create_quantile_sidxs(0,0,0);
  std::vector<t_sidx> exp {0};
  EXPECT_EQ(exp, sidxs);

  sidxs = pava.create_quantile_sidxs(0, 1, 0);
  exp = {0, 1};
  EXPECT_EQ(exp, sidxs);

  sidxs = pava.create_quantile_sidxs(2, 4, 0);
  exp = {2, 3, 4};
  EXPECT_EQ(exp, sidxs);
}

TEST(ModPava, CreateQuantileSidxsTwoQ) {
  auto  pava = PavaType(PavaType::QuantilesType({0.5, 0.8}));

  auto sidxs = pava.create_quantile_sidxs(0,0,0);
  std::vector<t_sidx> exp {0};
  EXPECT_EQ(exp, sidxs);

  sidxs = pava.create_quantile_sidxs(0,0,1);
  std::vector<t_sidx> exp2 {0, 1};
  EXPECT_EQ(exp2, sidxs);

  sidxs = pava.create_quantile_sidxs(0, 2, 0);
  std::vector<t_sidx> exp3 = {0, 2, 4};
  EXPECT_EQ(exp3, sidxs);

  sidxs = pava.create_quantile_sidxs(0, 2, 1);
  std::vector<t_sidx> exp4 = {0, 1, 2, 3, 4, 5};
  EXPECT_EQ(exp4, sidxs);

  sidxs = pava.create_quantile_sidxs(2, 4, 0);
  std::vector<t_sidx> exp5 = {4, 6, 8};
  EXPECT_EQ(exp5, sidxs);
}

TEST(ModPava, ReorderQuantiles) {
  // If midvalue is not passed explicitely, expect first number in upper half to be selected as midvalue
  auto  pava = PavaType(PavaType::QuantilesType({1, 0.5, 0.8, 0.3, 1}));
  EXPECT_EQ(PavaType::QuantilesType({0.8, 1, 0.3, 0.5}), pava.quantiles);

  pava = PavaType(PavaType::QuantilesType({1, 0.5, 0.8, 0.3, 1, 0.2}));
  EXPECT_EQ(PavaType::QuantilesType({0.5, 0.8, 1, 0.2, 0.3}), pava.quantiles);

  pava = PavaType(PavaType::QuantilesType({1, 0.5, 0.8, 0.3, 1}), 0.5);
  EXPECT_EQ(PavaType::QuantilesType({0.5, 0.8, 1, 0.3}), pava.quantiles);

  pava = PavaType(PavaType::QuantilesType({1, 0.5, 0.8, 0.3, 1}), 0.1);
  EXPECT_EQ(PavaType::QuantilesType({0.3, 0.5, 0.8, 1}), pava.quantiles);

  pava = PavaType(PavaType::QuantilesType({1, 0.5, 0.8, 0.3, 1}), 0.3);
  EXPECT_EQ(PavaType::QuantilesType({0.3, 0.5, 0.8, 1}), pava.quantiles);

  pava = PavaType(PavaType::QuantilesType({1, 0.5, 0.8, 0.3, 1}), 1.f);
  EXPECT_EQ(PavaType::QuantilesType({1, 0.3, 0.5, 0.8}), pava.quantiles);

}

TEST(ModPava, ModPavaMergeBackOneStep) {
  auto  pava = PavaType(PavaType::QuantilesType({0.5}));
  t_sidx last_sidx;

  // Build sections interval with three sections/blocks one observation each.
  // Note that this configuration can not occur with one single quantile since
  // obs in sections 1 and 2 are both preceded by section at 0 and in the natural
  // construction of the algorithm each quantile's sections preced their own, hence
  // if only one quantile, then all sections have sucessive previous sections
  QuantileBlock<ListOb> block1(nneg, 0.5);
  block1.add_ob(ListOb({0}, 10));
  pava.sections_interval.set_next_section(0, block1);

  QuantileBlock<ListOb> block2(nneg, 0.5);
  block2.add_ob(ListOb({0, 1}, 0));
  pava.sections_interval.set_next_section(0, block2);

  QuantileBlock<ListOb> block3(nneg, 0.5);
  block3.add_ob(ListOb({0, 2}, 5));
  pava.sections_interval.set_next_section(0, block3);

  std::vector<std::vector<t_value> > results;
  pava.extract_results(results);

  // Test blocks have expected values
  std::vector<std::vector<t_value> > expected;
  expected.push_back(std::vector<t_value>({10, -10, -5}));
  EXPECT_EQ(expected, results);

  // Merge last section only
  last_sidx = pava.sections_interval.merge_back(2);
  EXPECT_EQ(0, last_sidx);

  // Merge second section only
  last_sidx = pava.sections_interval.merge_back(1);
  EXPECT_EQ(0, last_sidx);

  results.clear();
  expected.clear();
  pava.extract_results(results);
  expected.push_back(std::vector<t_value>({5, 0, 0}));
  EXPECT_EQ(expected, results);
}

TEST(ModPava, ModPavaMergeUntilFirstSectionStartingLast) {
  auto  pava = PavaType(PavaType::QuantilesType({0.5}));
  t_sidx last_sidx;
  bool satisfy;

  // Build sections interval with three sections/blocks one observation each.
  // Note that this configuration can not occur with one single quantile since
  // obs in sections 1 and 2 are both preceded by section at 0 and in the natural
  // construction of the algorithm each quantile's sections preced their own, hence
  // if only one quantile, then all sections have sucessive previous sections
  QuantileBlock<ListOb> block1(nneg, 0.5);
  block1.add_ob(ListOb({0}, 10));
  pava.sections_interval.set_next_section(0, block1);

  QuantileBlock<ListOb> block2(nneg, 0.5);
  block2.add_ob(ListOb({0, 1}, 0));
  pava.sections_interval.set_next_section(0, block2);

  QuantileBlock<ListOb> block3(nneg, 0.5);
  block3.add_ob(ListOb({0, 2}, 5));
  pava.sections_interval.set_next_section(0, block3);

  std::vector<std::vector<t_value> > results;
  pava.extract_results(results);

  // Test blocks have expected values
  std::vector<std::vector<t_value> > expected;
  expected.push_back(std::vector<t_value>({10, -10, -5}));
  EXPECT_EQ(expected, results);

  // Merge last section, it should merge with section at 0 and become a valid block, so stop
  satisfy = pava.sections_interval.merge_back_until_first_or_satisfy_constraints(2, last_sidx);
  EXPECT_EQ(0, last_sidx);
  EXPECT_TRUE(satisfy);
  results.clear();
  expected.clear();
  pava.extract_results(results);
  expected.push_back(std::vector<t_value>({10, -10, 0}));
  EXPECT_EQ(expected, results);

  // Trying to merge back again should trigger invalid section
  ASSERT_THROW(pava.sections_interval.merge_back_until_first_or_satisfy_constraints(2, last_sidx), std::logic_error);

  // Merge section 1
  satisfy = pava.sections_interval.merge_back_until_first_or_satisfy_constraints(1, last_sidx);
  EXPECT_EQ(0, last_sidx);
  EXPECT_TRUE(satisfy);
  results.clear();
  expected.clear();
  pava.extract_results(results);
  expected.push_back(std::vector<t_value>({5, 0, 0}));
  EXPECT_EQ(expected, results);

  // Merge back section 1 once again, should trigger invalid section
  ASSERT_THROW(pava.sections_interval.merge_back_until_first_or_satisfy_constraints(1, last_sidx), std::logic_error);
}

TEST(ModPava, ModPavaMergeUntilFirstSectionStartingSecond) {
  auto  pava = PavaType(PavaType::QuantilesType({0.5}));
  t_sidx last_sidx;
  bool satisfy;

  // Build sections interval with three sections/blocks one observation each.
  // Note that this configuration can not occur with one single quantile since
  // obs in sections 1 and 2 are both preceded by section at 0 and in the natural
  // construction of the algorithm each quantile's sections preced their own, hence
  // if only one quantile, then all sections have sucessive previous sections
  QuantileBlock<ListOb> block1(nneg, 0.5);
  block1.add_ob(ListOb({0}, 10));
  pava.sections_interval.set_next_section(0, block1);

  QuantileBlock<ListOb> block2(nneg, 0.5);
  block2.add_ob(ListOb({0, 1}, 0));
  pava.sections_interval.set_next_section(0, block2);

  QuantileBlock<ListOb> block3(nneg, 0.5);
  block3.add_ob(ListOb({0, 2}, 5));
  pava.sections_interval.set_next_section(0, block3);

  std::vector<std::vector<t_value> > results;
  pava.extract_results(results);

  // Test blocks have expected values
  std::vector<std::vector<t_value> > expected;
  expected.push_back(std::vector<t_value>({10, -10, -5}));
  EXPECT_EQ(expected, results);

  // Merge last section, it should merge with section at 0 and become a valid block, so stop
  satisfy = pava.sections_interval.merge_back_until_first_or_satisfy_constraints(1, last_sidx);
  EXPECT_EQ(0, last_sidx);
  EXPECT_TRUE(satisfy);
  results.clear();
  expected.clear();
  pava.extract_results(results);
  expected.push_back(std::vector<t_value>({10, 0, -5}));
  EXPECT_EQ(expected, results);

  pava.print_sections_intervals();
  // Trying to merge back again, should trigger invalid section exception
  ASSERT_THROW(pava.sections_interval.merge_back_until_first_or_satisfy_constraints(1, last_sidx), std::logic_error);

  // Merge section 2
  satisfy = pava.sections_interval.merge_back_until_first_or_satisfy_constraints(2, last_sidx);
  EXPECT_EQ(0, last_sidx);
  EXPECT_TRUE(satisfy);
  results.clear();
  expected.clear();
  pava.extract_results(results);
  expected.push_back(std::vector<t_value>({5, 0, 0}));
  EXPECT_EQ(expected, results);

  // Merge back section 2 once again, should trigger invalid section exception
  ASSERT_THROW(pava.sections_interval.merge_back_until_first_or_satisfy_constraints(2, last_sidx), std::logic_error);
}

/*
TEST(BlocksTest, ModPavaIntervalMergeTwo) {
  typedef Pava<SectionsInterval<NNQuantileBlock, ListOb> > PavaType;
  auto  pava = PavaType(PavaType::QuantilesType({0.0, 0.1}));

  std::list<IntervalOb> obs_list = {
    IntervalOb({0, 1, 1}),
    IntervalOb({1, 2, 1}),
    IntervalOb({0, 3, 138}),
    IntervalOb({0, 4, 226}),
    IntervalOb({0, 5, 251}),
    IntervalOb({2, 6, 288}),
    IntervalOb({5, 7, 93}),
    IntervalOb({0, 8, 338}),
    IntervalOb({7, 9, 34}),
  };
  pava.do_pava(obs_list.begin(), obs_list.end());

//  EXPECT_EQ(5, block.calc_value(si));

  pava.print_sections_intervals();
}

TEST(BlocksTest, ModPavaIntervalMergeThree) {
  typedef Pava<SectionsInterval<NNQuantileBlock, ListOb> > PavaType;
  auto  pava = PavaType(PavaType::QuantilesType({0.0, 0.1}));

  std::list<IntervalOb> obs_list = {
    IntervalOb({2, 6, 288}),
    IntervalOb({5, 7, 93}),
    IntervalOb({0, 8, 338}),
    IntervalOb({7, 9, 34}),
  };
  pava.do_pava(obs_list.begin(), obs_list.end());

//  EXPECT_EQ(5, block.calc_value(si));

  pava.print_sections_intervals();
}*/
