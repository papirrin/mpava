#ifndef MOD_PAVA_HPP_INCLUDED
#define MOD_PAVA_HPP_INCLUDED

#include <list>
#include <functional>
#include "sections_interval.hpp"

template<class SectionsIntervalType> class Pava {

  public:

  typedef typename SectionsIntervalType::BlockType BlockType;
  typedef typename std::vector<float> QuantilesType;

  SectionsIntervalType sections_interval;
  QuantilesType orig_quantiles;
  QuantilesType quantiles;
  float midvalue;
  bool midvalue_set;
  size_t first_nonpositive_quantile;

  Pava(const QuantilesType & qs) : midvalue(-.1f), midvalue_set(false) {
    set_quantiles(qs);
  }
  Pava(const QuantilesType & qs, float midvalue) : midvalue(midvalue), midvalue_set(true) {
    set_quantiles(qs);
  }

  // If midvalue is not set, then it will be chosen to be the first value in the upper half of the
  // sorted unique vector of quantiles
  void set_quantiles(const QuantilesType & qs) {
    if (qs.empty()) {
      quantiles = QuantilesType({0.5});
      return;
    }
    quantiles = qs;

    // Sort, and deduplicate, then store original
    std::sort(quantiles.begin(), quantiles.end());
    auto last = std::unique(quantiles.begin(), quantiles.end());
    quantiles.erase(last, quantiles.end());
    orig_quantiles = qs;

    // Validate and find midvalue
    if (quantiles.front() < 0.0f || quantiles.back() > 1.0f)
      throw std::invalid_argument("All quantiles must be in [0,1] interval.");

    if (!midvalue_set) {
      midvalue = quantiles[quantiles.size()/2];
      midvalue_set = true;
    }
//    std::cout << "set_quantiles midvalue: " << midvalue << std::endl;
    assert(midvalue >= 0.f && midvalue <= 1.f);

    // Find closest to midvalue
    auto geq_than = [](float a, float b) -> float { return a >= b; };
    using namespace std::placeholders;

    auto it_first_more_than = std::stable_partition(quantiles.begin(), quantiles.end(), std::bind(geq_than, _1, midvalue));
/*    { // Debugging code
      std::cout << "reordered quantiles: " << "\t";
      for(auto q : quantiles) std::cout << q << " ,";
      std::cout << "is end: " << (it_first_more_than == quantiles.end()) << std::endl;
    }
*/
    first_nonpositive_quantile = std::distance(quantiles.begin(), it_first_more_than);
//    std::cout << "*************The first nonpositive quantile is at idx:" << first_nonpositive_quantile << " with quantiles.size(): " << quantiles.size() <<  std::endl;

    assert(first_nonpositive_quantile > 0);
  }

  t_sidx fill_with_empty_valid_regions(const t_sidx requested_sidx, t_sidx last_sidx) {
    if (requested_sidx < sections_interval.get_next_sidx())
      throw std::logic_error("Section index requested to fill up to is smaller than next section index.");
    while(sections_interval.get_next_sidx()  < requested_sidx) {
//      std::cout << "Inserting filler block at sidx:" << sections_interval.get_next_sidx() << " and last_sidx: " << last_sidx << std::endl;
      last_sidx = sections_interval.set_next_section_invalid_block(last_sidx, true);
    }
    return last_sidx;
 }

 t_sidx fill_with_empty_invalid_regions(const t_sidx requested_sidx) {
    if (requested_sidx < sections_interval.get_next_sidx())
      throw std::logic_error("Section index requested to fill up to is smaller than next section index.");
    t_sidx last_sidx = requested_sidx;
    while(sections_interval.get_next_sidx()  < requested_sidx) {
//      std::cout << "Inserting filler block at sidx:" << sections_interval.get_next_sidx() << " and same last_sidx." << std::endl;
      last_sidx = sections_interval.set_next_section_invalid_block(sections_interval.get_next_sidx(), false);
    }
    return last_sidx;
 }

  const t_sidx to_internal_sidx(const t_sidx sidx, const size_t qidx) const {
    return quantiles.size() * sidx + qidx;
  }

  const std::vector<t_sidx> create_quantile_sidxs(const t_sidx start, const t_sidx end, const size_t qidx) {
    // The way this function works requires first_nonpositive_quantile to be larger than zero.
    assert(qidx < quantiles.size());
    assert(first_nonpositive_quantile > 0);
    std::vector<t_sidx> sidxs;
    for(t_sidx i = start; i <= end; ++i) {
      // Always add the first quantile
      sidxs.push_back(to_internal_sidx(i, 0));

      for(t_sidx q = (qidx < first_nonpositive_quantile)?1:first_nonpositive_quantile; q <= qidx; ++q) {
        sidxs.push_back(to_internal_sidx(i, q));
      }
    }
    return sidxs;
  }

  template<class IteratorType> void do_pava(IteratorType it_begin, IteratorType it_end) {
    // Assume observations are sorted by (end, start). Every end denotes a section with one block.

    auto next_ob = *it_begin;
    bool first_ob = true;

    // each last_sidx denotes the last valid section that was added for that quantile
    std::vector<t_sidx> last_sidxs(quantiles.size());
    for( size_t qidx = 0; qidx < quantiles.size(); ++qidx)
      last_sidxs[qidx] = to_internal_sidx(next_ob.end, qidx);
    assert(last_sidxs.size() == quantiles.size());

    do {

      t_sidx curr_sidx = next_ob.end;

      std::list<IntervalOb> obs_to_add;

//      std::cout<< "Start  observation loop \t start:" << next_ob.start << "\t end: " << next_ob.end << "\t value: " << next_ob.value << std::endl;
      while(curr_sidx == next_ob.end) {
//        std::cout<< "Add observation   \t start:" << next_ob.start << "\t end: " << next_ob.end << "\t value: " << next_ob.value << std::endl;
        obs_to_add.push_back(next_ob);

        if(++it_begin != it_end)
          next_ob = *it_begin;
        else
          break;
      }

      for (size_t qidx = 0; qidx < quantiles.size(); ++qidx) {
        auto constraint = (qidx < first_nonpositive_quantile)? BlockType::NONNEGATIVE : BlockType::NONPOSITIVE;
//        std::cout << "Start iteration of adding observations for qidx: " << qidx << " with q: " << quantiles[qidx] << " constraint: " << constraint << std::endl;
        BlockType block(constraint, quantiles[qidx]);
        for (auto ob: obs_to_add) {
          auto ob_sidxs = create_quantile_sidxs(ob.start, ob.end, qidx);
/*
            {
              for(auto sidx : ob_sidxs) std::cout << sidx << " ,";
              std::cout << std::endl;
            }
*/
//          std::sort(ob_sidxs.begin(), ob_sidxs.end()); Should not sort after. create_quantile_sidxs should give them sorted already
//          std::unique(ob_sidxs.begin(), ob_sidxs.end());
          ListOb new_ob(ob_sidxs, ob.value);
//          std::cout << "Ob: " << new_ob << std::endl;
          block.add_ob(new_ob);
        }

        assert(block.has_obs());
        t_sidx next_sidx = to_internal_sidx(curr_sidx, qidx);
        if (sections_interval.get_next_sidx() > next_sidx) {
          // Verify section to create is at least expect sidx
          throw std::logic_error("Requested next section index is smaller than expected next section index. Did you forget to sort the observations?");
        }

        // Insert empty blocks as needed until the section gap is filled
        if (first_ob) {
          // No previous observation, these sections are invalid are just fillers, do not modify last_sidxs for
          // sections that contain observations that will be inserted later.
          fill_with_empty_invalid_regions(next_sidx);
          first_ob = false;
        } else {
//          last_sidxs[qidx] = fill_with_empty_valid_regions(next_sidx, last_sidxs[qidx]);
          fill_with_empty_valid_regions(next_sidx, last_sidxs[qidx]);
        }

        // Block filled up with observations. Test if should be added or should be merged.
//        std::cout << "Block filled. next_ob.end: " << next_ob.end << "\tlast_sidx: " << last_sidxs[qidx] << "\tblock value: " << block.calc_value(sections_interval) << "block constraint: " << block.constraint << std::endl;

        if (sections_interval.satisfy_constraints(block, (qidx < first_nonpositive_quantile)? unbounded_section: to_internal_sidx(curr_sidx, 0))) {
          // Is valid, it should be added
//          std::cout << "Adding valid block at " << sections_interval.get_next_sidx()  << std::endl;
          last_sidxs[qidx] = sections_interval.set_next_section(last_sidxs[qidx], block);
//          std::cout << "Finished adding valid block at " << sections_interval.get_next_sidx() - 1  << std::endl;
        } else {
//          std::cout << "merging invalid block" << std::endl;

          // Invalid block, should be merged with last inserted to avoid inserting it unnecesarily
          sections_interval.merge_block(last_sidxs[qidx], block);
//          std::cout << "merging until satisfy constraints" << std::endl;
          bool satisfy_constraints = sections_interval.merge_back_until_first_or_satisfy_constraints(last_sidxs[qidx], last_sidxs[qidx]);

          // Only sections that do not have previous can not satisfy constraints
          assert(satisfy_constraints || !sections_interval.section_has_prev(last_sidxs[qidx]));
//          std::cout << "constraints satisfied with block in section " << last_sidxs[qidx] << std::endl;
          sections_interval.set_next_section_invalid_block(last_sidxs[qidx], false);
        }
      } // end for loop to insert one section per quantile

    } while(it_begin != it_end);
//    std::cout << "**************End pava***************" << std::endl;
  }

  void print_sections_intervals() {
//    std::cout << "Sections Interval" << std::endl;
    auto & sections = sections_interval.get_sections();

    for(t_sidx sidx = 0; sidx < sections.size(); ++sidx) {
      // Go over each section
      auto s = sections[sidx];

      std::cout << "Section: " << sidx << "\tprev_sidx: " << s.prev_sidx  <<"\tis_valid: " << s.is_valid_section() << "\thas_valid_block: " << s.has_valid_block();

      if (s.has_valid_block()) {
        auto b = sections_interval.get_block(sidx);
        std::cout << "\tbidx: " << s.bidx << "block:" <<  b <<std::endl;
      } else {
        std::cout << std::endl;
      }

/*      if (s.has_valid_block()) {
        // Get block for that section
        auto obs_iter = b.obs_begin();

        while (obs_iter != b.obs_end()) {
          // Go over each observation in block
          ListOb o = *(obs_iter++);
          std::cout << "\tSidxs: " << o << std::endl;
        }
      }
*/
    }
    std::cout << "**********************" << std::endl;
  }

  // add values in sections to results, one list per quantile. Return nr_sections.
  template <typename ValueType> size_t extract_results(std::vector<std::vector<ValueType> > & results) {

    assert (results.size() == 0); //TODO: do i want to add or do something if results is not empty
    assert(orig_quantiles.size() == quantiles.size());

    auto & si = sections_interval;
    const size_t nr_qs = quantiles.size();

    assert(si.get_sections().size() % nr_qs == 0);
    const size_t nr_sections = si.get_sections().size() / nr_qs;

    results.resize(nr_qs);

    std::vector<size_t> to_orig_quantiles;
    for (auto q : quantiles) {
      auto it = std::find(orig_quantiles.begin(), orig_quantiles.end(), q);
      assert(it != orig_quantiles.end());
//      std::cout << " intersting to_quantile q: " << *it << " \t qidx: " << it - orig_quantiles.begin() << std::endl;
      to_orig_quantiles.push_back( it - orig_quantiles.begin());
    }
    assert(to_orig_quantiles.size() == quantiles.size());

    // Inner loop in quantiles is more cache friendly
    for (t_sidx input_sidx = 0; input_sidx < nr_sections; ++input_sidx) {
      for (size_t qidx = 0; qidx < nr_qs; ++qidx) {

      auto & q_results = results[to_orig_quantiles[qidx]];

//        std::cout << "****************** sidx: " << to_internal_sidx(input_sidx, qidx) << std::endl;
        t_sidx sidx = to_internal_sidx(input_sidx, qidx);
        ValueType v;
        si.calc_value(sidx, v);

        // qidx == 0 has no prev value, if qidx == first_nonpositive_quantile prev value is first nonnegative, otherwise prev is qidx-1
        size_t internal_qidx = (qidx == first_nonpositive_quantile)? 0 : qidx - 1;
        ValueType v_prev_q = (qidx > 0) ? results[to_orig_quantiles[internal_qidx]][input_sidx] : 0;
//        std::cout << "****************** v: " << v << "\tv_prev_q: " << v_prev_q << std::endl;
        q_results.push_back(v + v_prev_q);
      }
    }
    return nr_sections;
  }
};
#endif
