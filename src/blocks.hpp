#ifndef BLOCKS_HPP_INCLUDED
#define BLOCKS_HPP_INCLUDED

#include <boost/algorithm/clamp.hpp>
#include "observations_containers.hpp"


// Non-negative Quantile Block
template <class ObType> class QuantileBlock {
  QuantileContainer<ObType> obs_cont;

  public:

  enum Constraint{
    NONNEGATIVE,
    NONPOSITIVE
  };

  const Constraint constraint;

  static constexpr t_value base_value(){ return 0; }

  QuantileBlock(QuantileBlock::Constraint constraint): constraint(constraint) {}
  QuantileBlock(QuantileBlock::Constraint constraint, const float q) : QuantileBlock(constraint) {set_quantile(q);}

  void set_quantile(const float q) {
    obs_cont.q = q;
  }

  bool has_obs() const {
    return !obs_cont.obs.empty();
  }

  size_t nr_obs() const {
    return obs_cont.obs.size();
  }

  auto obs_begin() { return obs_cont.obs.begin(); }
  auto obs_end() { return obs_cont.obs.end(); }

  void add_ob(const ObType & ob) {
    obs_cont.add_ob(ob);
  }

  void transfer_obs(QuantileBlock<ObType> & block){
    obs_cont.obs.insert(obs_cont.obs.end(), block.obs_cont.obs.begin(), block.obs_cont.obs.end());
    block.obs_cont.obs.clear();
  }

  template<class IteratorType> void add_obs(IteratorType it_start, const IteratorType & it_end) {
    while (it_start != it_end) {
      add_ob(*it_start);
      it_start ++;
    }
  }

  template<class BlocksIntervalType> t_value calc_value(BlocksIntervalType & bi, const t_sidx upper_bound=std::numeric_limits<t_sidx>::max()) {
    assert(has_obs());
    return obs_cont.calc_value(bi, upper_bound);
  }
/*
  template<class BlocksIntervalType> bool satisfy_constraints(BlocksIntervalType & bi, t_sidx bound_sidx) {
    assert(has_obs());
//    std::cout << "block::is_valid:" << calc_value(bi) << std::endl;
    if (bound_sidx == unbounded_section) {
      return is_value_valid(calc_value(bi));
    } else {
      assert(bi.section_is_valid(bound_sidx));
      t_value bound_value;
      bi.calc_value(bi.sections[bound_sidx], bound_value);
      return is_value_valid(calc_value(bi), bound_value);
    }
  }
*/
  // TODO: Make this no static, and always use the block to check for validity. That will allow to change the direction of the inqueality per block
  // and will be useful to estimate quantiles around arbiratry ones (probably median)
  bool is_value_valid(const t_value v) const {
    return (constraint == NONNEGATIVE)?v >= base_value() : v <= base_value();
  }

  bool is_value_valid(const t_value v, const t_value abs_bound) const {
    const bool satisfy_constraint = (constraint == NONNEGATIVE)?v >= base_value() : v <= base_value();
    return (satisfy_constraint && (std::abs(v) <= std::abs(abs_bound)));
  }

  t_value closest_valid_value(const t_value v) const {
    return (constraint == NONNEGATIVE)? std::max(base_value(), v) : std::min(base_value(), v);
  }

  t_value closest_valid_value(const t_value v, const t_value abs_bound) const {
    t_value abs_value = boost::algorithm::clamp(std::abs(v), base_value(), std::abs(abs_bound));
    return (constraint == NONNEGATIVE)? abs_value: -abs_value;
  }

  std::string as_yaml(short unsigned int ident_level = 0) const {

    auto ident = std::string(ident_level, ' ');

    std::string obs_yaml = join( obs_cont.obs |
        transformed( [](ObType ob)-> std::string { return ob.as_yaml(0);} ), ",");

    return str(format("%s{ obs:%s }") % ident % obs_yaml);
  }

};

template<typename ObType> std::ostream& operator<< (std::ostream& os, const QuantileBlock<ObType> & block){
  return os << block.as_yaml(0);
}

#endif
