#ifndef OBSERVATIONS_HPP_INCLUDED
#define OBSERVATIONS_HPP_INCLUDED

#include <vector>
#include "common.hpp"


// (start, end) determine the interval of the sections that this observation covers, inclusive.
struct IntervalOb {
  t_sidx start, end;
  t_value value;
  IntervalOb(const t_sidx start, const t_sidx end, const t_value value) : start(start), end(end), value(value){}
};


struct ListOb {
  std::vector<t_sidx> sidxs; // Increasingly ordered list of sections indexes
  t_value value;
  ListOb(const std::vector<t_sidx> & sidxs_, const t_value value) : sidxs(sidxs_), value(value){
    // Assert that all are unique
    assert(std::is_sorted(sidxs.begin(), sidxs.end()));
    assert(std::unique(sidxs.begin(), sidxs.end()) == sidxs.end());
  }

  /*
   *  Printing
   */

  std::string as_yaml(short unsigned int ident_level = 0) const {
    
    auto ident = std::string(ident_level, ' ');
    std::string sidxs_csv = join( sidxs | 
        transformed( static_cast<std::string(*)(t_sidx)>(std::to_string) ), ",");

    return str(format("%s{ sidxs: [%s], value: %s }") % ident % sidxs_csv % value);
  }
};

std::ostream& operator<< (std::ostream& os, const ListOb & ob);

#endif
