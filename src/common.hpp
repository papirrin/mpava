#ifndef COMMON_HPP_INCLUDED
#define COMMON_HPP_INCLUDED

#include <assert.h>
//#define NDEBUG 
#include <ostream>
#include <boost/format.hpp>
#include <boost/algorithm/string/join.hpp>
#include <boost/range/adaptor/transformed.hpp>


// Type for values
typedef int t_value;
typedef size_t t_bidx; // Type for block indexes. It can be made smaller to improve cache performance.
typedef size_t t_sidx; // Type for section indexes. It can be made smaller to improve cache performance.


using boost::format;
using boost::adaptors::transformed;
using boost::algorithm::join;

#endif
