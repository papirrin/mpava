#ifndef OBSERVATIONS_CONTAINER_HPP_INCLUDED
#define OBSERVATIONS_CONTAINER_HPP_INCLUDED

#include "common.hpp"
#include "observations.hpp"

#include <vector>
#include <algorithm>
#include <math.h>


template <class ObType> struct QuantileContainer {
  std::vector<ObType> obs;
  std::vector<t_value> values;

  // Quantile to use in Block's value calculations. Initialize to invalid value to prevent usage of unitialized instances 
  float q = -1;

  // Rank corresponding to such quantile in a decreasing order
  float ob_rank(){
    return round((obs.size()-1)*(q));
  }
  void add_ob(const ObType & ob) {
    obs.push_back(ob);
  }

  // TODO: Move this to block
  template <class BlocksIntervalType> t_value calc_value(BlocksIntervalType & bi, const t_sidx upper_bound) {
    assert(obs.size() > 0); // It makes no sense to ask value of block of size 0
    assert(q >= 0 && q <= 1); // invalid quantile value

    // TODO: This is an extremely inefficient implementation. A dynamic programming cache of previous solutions would help
    values.clear();
    for(auto & ob : obs){
      values.push_back(bi.calc_value_preceding_sections(ob, upper_bound));
    }
    // Sort in decreasing order
    std::sort(values.begin(), values.end());
/*    for(auto v :values)
      std::cout<<v << ", ";
    std::cout << std::endl;
    std::cout << "rank: " << ob_rank() << std::endl;
*/
    return values[ob_rank()];
  }
};

#endif
