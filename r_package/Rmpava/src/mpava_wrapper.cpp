#include <Rcpp.h>
#include <boost/format.hpp>
#include "mod_pava.hpp"
using namespace Rcpp;

// [[Rcpp::export]]
List rcpp_qnnreg(NumericVector qs, NumericVector start, NumericVector end, NumericVector value) {

  std::vector<float>  c_qs    = as< std::vector<float> >(qs);
  std::vector<t_sidx> c_start = as< std::vector<t_sidx> >(start);
  std::vector<t_sidx> c_end   = as< std::vector<t_sidx> >(end);
  std::vector<t_value> c_value = as< std::vector<t_value> >(value);

  // TODO: Make it an exception?
  assert(c_start.size() == c_end.size() == c_value.size());
  
  t_sidx size = c_start.size();

  // Create vector with observations
  std::vector<IntervalOb> obs;
  obs.reserve(size);

  for(t_sidx sidx = 0; sidx < size; ++sidx)
    obs.push_back(IntervalOb(c_start[sidx], c_end[sidx], c_value[sidx]));

  typedef Pava<SectionsInterval<NNQuantileBlock, ListOb> > PavaType;
  auto  pava = PavaType(c_qs);
  pava.do_pava(obs.begin(), obs.end());

  std::vector<std::vector<t_value> > results;
  size_t nr_sections = pava.extract_results(results);
  size_t nr_qs = results.size();

  auto l = List();

  for(auto q_results: results) {
    l.push_back(q_results);
  }

  StringVector col_names(nr_qs);
  for(size_t qidx; qidx < nr_qs; ++qidx)
    col_names[qidx] = (boost::format("q.%d") % (c_qs[qidx] * 100)).str();

  l.attr("names") = col_names;
  
  return l;
}
