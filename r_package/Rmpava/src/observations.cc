#include "observations.hpp"

std::ostream& operator<< (std::ostream& os, const ListOb & ob) {
  return os << ob.as_yaml(0);
}
