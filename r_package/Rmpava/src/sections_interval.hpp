#ifndef SECTIONS_INTERVAL_HPP_INCLUDED
#define SECTIONS_INTERVAL_HPP_INCLUDED

#include <vector>
#include <limits>
#include <sstream>

#include "blocks.hpp"

 // An invalid section must also have an invalid block, these arrangement makes that check doable by a range check
 static const t_bidx invalid_section = std::numeric_limits<t_bidx>::max();
 static const t_bidx invalid_bidx = invalid_section - 1;

 static const t_sidx unbounded_section = std::numeric_limits<t_sidx>::max();

// Section State with arbitrary prev block.
// Cache efficiency is greatly dependent on the size of SectionState
class SectionState {


  public:
  t_bidx bidx;
  t_sidx prev_sidx;
  t_sidx bound_sidx;

  SectionState(t_bidx bidx, t_sidx prev_sidx, t_sidx bound_sidx, const bool is_valid=true) : bidx(bidx), prev_sidx(prev_sidx), bound_sidx(bound_sidx) {
    if(is_valid) {
      assert(is_valid_section());
    } else {
      assert(!has_valid_block());
      set_section_invalid();
    }
  }

  SectionState(t_sidx prev_sidx, t_sidx bound_sidx, const bool is_valid=true) : SectionState(invalid_bidx, prev_sidx, bound_sidx, is_valid) {
  }

  void set_section_invalid() {
    bidx = invalid_section;
  }

  void set_block_invalid() {
    bidx = invalid_bidx;
  }

  // An invalid section implies an invalid block
  bool is_valid_section() const {
    return bidx < invalid_section;
  }

  // An invalid block does not imply an invalid section
  bool has_valid_block() const {
    return bidx < invalid_bidx;
  }

  bool has_valid_bound() const {
    return bound_sidx != unbounded_section;
  }
};

// Section indexes are local to each sections interval, they always start with 0 within each instance
template <template<class> class BlockTypename, class ObType, bool cache_values = true> class SectionsInterval {
  public:
  typedef BlockTypename<ObType> BlockType;

  protected:
  std::vector<BlockType> blocks;
  std::vector<SectionState> sections;

  // Section values are indexed using bidx, hence they do not store values for invalid sections or sections with invalid blocks.
  // Previously calculated section values are stored.
  // When blocks in sections are merged, the next_section_value_sidx is decreased to the sidx of the new value
  // invalidating all the previosly stored section_values with sidx larger than next_section_value_sidx .
  std::vector<t_value> section_values;
  t_sidx next_section_value_sidx; // Defined such that zero means no valid_value.

  // A cached value is valid if it has been set before and
  inline void section_set_value(const t_sidx sidx, const t_value v) {
    assert(next_section_value_sidx == sidx);
    assert(section_has_valid_block(sidx));
    section_values[sections[sidx].bidx] = v;
    next_section_value_sidx++;
  }

  // Change next_section_value_sidx as neccesary after a block modification
  inline void adjust_next_section_value(const t_sidx modified_sidx) {
    assert(section_has_valid_block(modified_sidx));
    next_section_value_sidx = std::min(next_section_value_sidx, modified_sidx);
  }

  inline bool section_is_valid(const t_sidx sidx) const {
    assert(sidx < sections.size());
    return sections[sidx].is_valid_section();
  }

  inline bool section_has_valid_block (const t_sidx sidx) const {
    assert(sidx < sections.size());
    const bool has_valid_block = sections[sidx].has_valid_block();
    assert(!has_valid_block || (has_valid_block && get_block(sidx).has_obs())); // A block can only be valid if has observations in it. Don't try to get_block if has_valid_block is false
    return has_valid_block;
  }

  public:

  // The previous section must have a smaller sidx (or same if no previous section)
  inline bool section_has_prev(const t_sidx sidx) const {
    assert(sections[sidx].prev_sidx <= sidx);
    return sections[sidx].prev_sidx < sidx;
  }

  SectionsInterval() : next_section_value_sidx(0) {
  }

  SectionsInterval(const t_sidx nr_sections) : SectionsInterval() {
    reserve(nr_sections);
  }

  auto & get_sections() { //TODO: Hide this
    return sections;
  }

  // Inclusive interval of sections indexes that this instance handles
  void reserve(const t_sidx nr_sections) {
    // It makes no sense to hold no sections
    assert(nr_sections > 0);
    // Verify that indexes can hold this many sections
    assert(nr_sections <= std::numeric_limits<t_bidx>::max() - 1);

    // TODO: Choose appropiate values for reserve
    sections.reserve(nr_sections);
    blocks.reserve(nr_sections / 2);
    section_values.reserve(nr_sections / 2);
  }

  // Blocks must be either valid or have a valid prev_block
  inline BlockType & get_block(const t_sidx sidx) {
//    std::cout << "Getting block for sidx: " << sidx << std::endl;
    auto & ss = sections[sidx];
    assert(ss.is_valid_section());
    assert(ss.has_valid_block());
    return blocks[ss.bidx];
  }

  // Blocks must be either valid or have a valid prev_block
  inline const BlockType & get_block(const t_sidx sidx) const{
//    std::cout << "Getting block for sidx: " << sidx << std::endl;
    auto & ss = sections[sidx];
    assert(ss.is_valid_section());
    assert(ss.has_valid_block());
    return blocks[ss.bidx];
  }

  inline const t_sidx get_next_sidx() const {
    return sections.size();
  }

  // Set new section at next position available. Return sidx of new section.
  const t_sidx set_next_section(const t_sidx prev_sidx, const BlockType & block, const t_sidx bound_section = unbounded_section ) {
    assert(prev_sidx <= get_next_sidx());
    sections.push_back(SectionState(blocks.size(), prev_sidx, bound_section));
    blocks.push_back(block);
    section_values.resize(blocks.size());
    const t_sidx new_sidx = sections.size() - 1;
    if (cache_values)
      adjust_next_section_value(new_sidx);
    return new_sidx;
  }

  const t_sidx set_next_section_invalid_block(const t_sidx prev_sidx, const bool as_valid) {
//    std::cout << "set_next_section_invalid_block. prev_sidx: " << prev_sidx << " next_sidx:" << get_next_sidx() << std::endl;
    assert(prev_sidx <= get_next_sidx());
    sections.push_back(SectionState(prev_sidx, as_valid));
    const t_sidx new_sidx = sections.size() - 1;
    return new_sidx;
  }

  // Merges block into a section
  inline void merge_block(const t_sidx target_sidx, BlockType & block) {
    get_block(target_sidx).transfer_obs(block);
    adjust_next_section_value(target_sidx);
  }

  // Merge source with its prev_section, return prev_section
  inline const t_sidx merge_back(const t_sidx source_sidx) {
    BlockType & block_source = get_block(source_sidx);
    assert(section_has_prev(source_sidx));

    t_sidx dest_sidx = sections[source_sidx].prev_sidx;
 //   std::cout << "do section_has_valid_block with source_sidx: " << source_sidx << " and: " <<dest_sidx << std::endl;
    assert(section_is_valid(dest_sidx));
    assert(section_has_valid_block(dest_sidx));

//    std::cout << "do get_block" << std::endl;
    BlockType & block_dest = get_block(dest_sidx);

//    std::cout << "transfer" << std::endl;
    block_dest.transfer_obs(block_source);

    sections[source_sidx].set_section_invalid();
    adjust_next_section_value(dest_sidx);
    return dest_sidx;
  }

  // Merge with previous sections starting with begin until the block in section is valid.
  // Output last section that was merged in last_sidx parameter. Return true or false if satisfy_constraints.
  // Sections that are merged into anothers are invalidated as well as their blocks.
  // In the general case, t is not guaranteed that merging back will yield a block that satisfies the constraints
  // i.e. Two quantiles where the largest one violates but has no valid prev_section to merge with.
  const bool merge_back_until_first_or_satisfy_constraints(const t_sidx begin, t_sidx & last_sidx) {

    if (!section_has_valid_block(begin)) {
      std::stringstream s;
      s << "The section index: " << begin << " requested to begin merge on is invalid or has an invalid block.";
      throw std::logic_error(s.str());
    }

    // Firstly merge observations until blocks are valid. Then update prev_section for each section modified.
    t_sidx source_sidx = begin;
    t_sidx dest_sidx = begin;

    t_value bogus_value; // variable to hold by reference return from calc_value

    // Transfer observations to previous
    bool satisfy_constraints = calc_value(source_sidx, bogus_value);
//    std::cout << "Block at sidx: " << begin << " potentially violates constraint with value:" << bogus_value  << std::endl;

    while (!satisfy_constraints && section_has_prev(source_sidx)) {
      dest_sidx = merge_back(source_sidx);
//      std::cout << "do satisfy_constraints" << std::endl;
      satisfy_constraints = calc_value(dest_sidx, bogus_value);
//      std::cout << "after satisfy_constraints" << bogus_value << std::endl;
      source_sidx = dest_sidx;
    }

    // after this loop, source_sidx has the id of the last block
    t_sidx curr_sidx = begin;
    last_sidx = dest_sidx;

    do {
      auto & ss = sections[curr_sidx];
//      std::cout << "Changing prev_sidx at: " << curr_sidx << " from: " << ss.prev_sidx << " to: " << last_sidx <<  std::endl;
      ss.prev_sidx = last_sidx;

      curr_sidx = ss.prev_sidx;
    } while (curr_sidx != last_sidx);
//    std::cout << "******End changing prev_sidx loop *****" << std::endl;

    return satisfy_constraints;
  }

  bool satisfy_constraints(BlockType & block, t_sidx bound_sidx) {
    assert(block.has_obs());
//    std::cout << "sections_interval::satisfy_constraints, bound_sidx: " << bound_sidx  << std::endl;
    if (bound_sidx == unbounded_section) {
//      std::cout << "unbounded" << std::endl;
      return block.is_value_valid(block.calc_value(*this));
    } else {
      assert(section_is_valid(bound_sidx));
      t_value bound_value;
      calc_value(bound_sidx, bound_value);
//      std::cout << "bounded with bound_value: " << bound_value << std::endl;
      return block.is_value_valid(block.calc_value(*this), bound_value);
    }
  }

  // Retrieve valid value from values or calculate new one. Cached them if cache_values==true
  // Return whether block value satisfy constraints or not (not section value). The section value is set in reference argument output_value
  // Value of a section is defined to be it's block value iff has no previous section, otherwise it's max(BlockType::min_value, block value)
  inline bool calc_value(const t_sidx sidx, t_value & output_value) {
    // Obtain bidx or return min_value for blocks
    if (section_has_valid_block(sidx)) {
      const t_bidx bidx = sections[sidx].bidx;
      // Recalculate value for current bidx
      auto & b = get_block(sidx);
      assert(b.has_obs());

      if (cache_values && sidx < next_section_value_sidx) {
        const t_value section_value = section_values[bidx];
        output_value = section_value;
      } else {
        // Recalculate all values until requested sidx
        while (cache_values && next_section_value_sidx < sidx) {
          // Since values on sections with invalid block are not counted, skip them
          if(section_has_valid_block(next_section_value_sidx)) {
            t_value prev_section_value;
            calc_value(next_section_value_sidx, prev_section_value); // This will increase next_valid_value_bidx until reaching sidx
          } else {
            ++next_section_value_sidx;
          }
        }

        auto block_value = b.calc_value(*this, sidx);
        if (section_has_prev(sidx)) {
          output_value = block_value;
        } else if (sections[sidx].has_valid_bound()) {
          t_value bound_value;
          calc_value(sections[sidx].bound_sidx, bound_value);
          output_value = b.closest_valid_value(block_value, bound_value);
        } else {
          output_value = b.closest_valid_value(block_value);
        }

        if (cache_values) {
          section_set_value(sidx, output_value);
        }
      }
//    std::cout << "calc_value with valid block output_value: " << output_value << std::endl;
      if (sections[sidx].has_valid_bound()) {
        t_value bound_value;
        calc_value(sections[sidx].bound_sidx, bound_value);
        return b.is_value_valid(output_value, bound_value);
      } else {
        return b.is_value_valid(output_value);
      }

    } else {
      output_value = BlockType::base_value();
      return true;
    }
  }

  t_value calc_value_preceding_sections(const IntervalOb & ob, const t_sidx upper_bound) {
//    std::cout << "BlocksInterval::calc_last_block_value for ob: " << ob  << std::endl;
    if (ob.start == ob.end)
      return ob.value;

//    std::cout << "BlocksInterval::calc_last_block_value::calculating... " << std::endl;
    t_value curr_value = ob.value;

    // Add values of previous blocks
    t_sidx max_sidx = std::min(ob.end, upper_bound);
    for (t_sidx sidx = ob.start; sidx < max_sidx; ++sidx) {
      t_value val;
      calc_value(sidx, val);
      curr_value -= val;
    }

//    std::cout << "BlocksInterval::calc_last_block_value::returning" << std::endl;
    return curr_value;
  }

  t_value calc_value_preceding_sections(const ListOb & ob, const t_sidx upper_bound) {
//    std::cout << "BlocksInterval::calc_last_block_value for ob: " << ob << std::endl;
    assert(ob.sidxs.size() > 0);
    if (ob.sidxs.size() == 1)
      return ob.value;

//    std::cout << "BlocksInterval::calc_last_block_value::calculating... " << std::endl;
    t_value curr_value = ob.value;
    // Add values of previous blocks

    for (auto it = ob.sidxs.begin(); it != ob.sidxs.end() - 1 && *it < upper_bound; ++it) {
      t_sidx sidx = *it;
      t_value val;
      calc_value(sidx, val);
      curr_value -= val;
    }

//    std::cout << "BlocksInterval::calc_last_block_value::returning" << std::endl;
    return curr_value;
  }

  const size_t nr_obs(const t_sidx sidx) const {
    return (section_has_valid_block(sidx)) ? get_block(sidx).nr_obs() : 0;
  }
};
#endif
